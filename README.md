# umb-messenger

Webhook responsible for UMB message sending. Messages can be sent upon
completion of the specified stage or finished pipelines.

## Currently supported messages

* **OSCI:** For details about schema and use case, check out [Fedora CI messages].
  Messages are sent upon pipeline completion, if the pipeline was configured to
  support the messages.

  **BE SURE TO RUN THE MESSAGE VALIDATION BEFORE CHANGING ANYTHING IN THE MESSAGES!**
  The validation script and guidelines are available [on Pagure].

* **Ready for test:** For details about testing CKI builds and submitting
  results to CKI, see the [CKI documentation]

## How to run this project

The messenger can be deployed locally by running

```bash
python -m umb_messenger --queue
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

- `SSL_PEM_PATH`: UMB certificate, defaults to `/etc/cki/umb/cki-bot-prod.pem`
- `UMB_CONFIG_PATH`: UMB configuration, defaults to `/etc/cki/umb-config/umb.yml`
- `GITLAB_TOKENS`: JSON dictionary mapping GitLab host names to names of
  environment variables with the corresponding private tokens
- `COM_GITLAB_TOKEN` etc: private GitLab tokens
- `CKI_LOGGING_LEVEL`: logging level for CKI modules, defaults to WARN; to get
  meaningful output on the command line, set to INFO

To receive messages via `--queue` from RabbitMQ, the following environment
variables need to be set as well:

- `RABBITMQ_HOST`: space-separated list of AMQP servers
- `RABBITMQ_PORT`: AMQP port
- `RABBITMQ_USER`: user name of the AMQP account
- `RABBITMQ_PASSWORD`: password of the AMQP account
- `WEBHOOK_RECEIVER_EXCHANGE`: name of the webhook exchange, defaults to
  `cki.exchange.webhooks`
- `UMB_MESSENGER_ROUTING_KEYS`: routing keys corresponding to the subscribed
  webhook events

### Manual triggering

Messaging can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```
CKI_LOGGING_LEVEL=INFO \
  IS_PRODUCTION=true \
  python3 -m umb_messenger \
  --gitlab-url GITLAB_URL \
  --hook-type HOOK_TYPE \
  --id PIPELINE_ID \
  --project PROJECT_NAME
```

* `HOOK_TYPE` can be `job` or `pipeline`. The naming corresponds to mocked
  GitLab webhook. For parallel testing, use `job`. For post-testing messages
  (e.g. OSCI), use `pipeline`.
* `PROJECT_NAME` is a full project name (including the namespace) where the
  pipeline was executed.
* `PIPELINE_ID` is the pipeline ID.

### Production setup

If `IS_PRODUCTION` is set to `true`, [sentry] support is enabled via
`SENTRY_DSN`. You can the value in your sentry project's settings under
`SDK Setup / Client Keys`.


[Fedora CI messages]: https://pagure.io/fedora-ci/messages/
[CKI documentation]: https://cki-project.org/docs/test-maintainers/testing-builds
[receiver's documentation]: https://gitlab.com/cki-project/pipeline-trigger/#umb-result-message-format
[sentry]: https://sentry.io/welcome/
[on Pagure]: https://pagure.io/fedora-ci/messages
